# kumpulan configuration 
> Disusun oleh Hardiyanto

### Daftar Isi
* [Configuration]()
    * [Config bash](https://gitlab.com/dwiHard/LinuxAdministration/-/blob/master/LinuxConfigBackup/.bashrc)
    * [Config zsh](https://gitlab.com/dwiHard/LinuxAdministration/-/blob/master/LinuxConfigBackup/zshrc)
    * [Config vim](https://gitlab.com/dwiHard/LinuxAdministration/-/blob/master/vim/.vimrc)
* [Script]()
    * [Redirect](https://gitlab.com/dwiHard/LinuxAdministration/-/blob/master/LinuxConfigBackup/redirects.sh)
    * [File Server + MySQL Backup](https://gitlab.com/dwiHard/LinuxAdministration/-/blob/master/mysql/file+mysql-backup.bash)
    * [Mysql Backup](https://gitlab.com/dwiHard/LinuxAdministration/-/blob/master/mysql/mysql-backup.sh)
    * [Info MAC](https://gitlab.com/dwiHard/LinuxAdministration/-/blob/master/LinuxConfigBackup/mac-addresses.sh)
    * [Info NIC](https://gitlab.com/dwiHard/LinuxAdministration/-/blob/master/LinuxConfigBackup/nic-info.sh)
    * [Port Speed](https://gitlab.com/dwiHard/LinuxAdministration/-/blob/master/LinuxConfigBackup/port-speed.sh)
* [Python Script]()
    * [Decode Url](https://gitlab.com/dwiHard/LinuxAdministration/-/blob/master/LinuxConfigBackup/decodeUrl.py)
    * [Descrypt 12](https://gitlab.com/dwiHard/LinuxAdministration/-/blob/master/LinuxConfigBackup/decrypt12.py)
    * [Spam Chat Whatapps](https://gitlab.com/dwiHard/LinuxAdministration/-/blob/master/python/kumpulan%20script/spamChat.py)
