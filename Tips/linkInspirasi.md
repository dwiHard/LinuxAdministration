# Kumpulan link inspirasi Desain dan Pemograman
> disusun oleh Hardiyanto

#### Daftar Isi

* [Pemograman](#pemograman)
    * [Website](#website)
    * [DevOS](#devos)
    * [Android](#android)
* [Desain](#desain)
    * [Blender](#blender)
    * [UI/UX](#uiux)
    * [Mockup](#mockup)
    * [SVG](#svg)
    * [Animasi](#animasi)
    * [Background](#background)
    * [Warna Inspirasi](#warna-inspirasi)
    * [Tool Cepat](#tool-cepat)
    * [Video](#video)
<br>
</br>

### Pemograman
#### WEBSITE

- <https://getcssscan.com/css-box-shadow-examples> Getcsscan - CSS box shadow

- <https://medium.com/@snow_20352/20-creative-search-bar-design-inspirations-with-html-css-bootstrap-6717109fa0a0> Medium - CSS Search Bar

- <https://www.smashingmagazine.com/> Smashing Magazine - Tutorial

- <https://cssauthor.com/> CSS Author - Tutorial

- <https://www.tutorialspoint.com/index.htm> Tutorial Sploit - Tutorial

- <https://www.geeksforgeeks.org/> GeeksforGeeks - Tutorial

- <https://cssgenerator.org/> CSS Generator - Tutorial

<br>

- <https://animate.style/> Animate - CSS Animasi

- <https://www.minimamente.com/project/magic/> minimamente - CSS Animasi

- <http://vivify.mkcreative.cz/> vivify - CSS Animasi

- <https://kushagra.dev/lab/hint/> Hint - CSS Animasi

<br>
</br>

- <https://moderncss.dev/> Kushagra - CSS Soution

- <https://tobiasahlin.com/> Tobiasahlin - Desain web

<br>
</br>

- <https://any-api.com/> Any API - Kumpulan REST API KEY

- <https://github.com/public-apis/public-apis> Kumpulan Public API KEY

- <https://github.com/farizdotid/DAFTAR-API-LOKAL-INDONESIA> Kumpulan Public API KEY Local


#### DevOS 

- <https://www.geeksforgeeks.org/python-programming-language/?ref=grb> Geeks For Geeks - Python Testing

- <https://overapi.com/> Over API - Kumpulan link-link 

#### Android

- <https://support.testsigma.com/support/solutions/articles/32000028757-testsigma-changelog> Testsigma - Android Testing

- <http://adbcommand.com/awesome-adb> ADB command - ADB cheat sheet

- <https://github.com/kmvignesh/SplashScreen/tree/master/app/src/main/res/anim> Contoh Animasi Splash Screen Android

### Desain
#### Blender

- <https://polyhaven.com/all> Polyhaven - Assets Untuk Blender

- <https://www.textures.com/library> Textures - Assets Untuk Blender

#### UI/UX

- <https://uigarage.net> Ui Garde - UI/UX

- <https://www.uistore.design/> Ui Store - UI/UX

- <https://www.dwinawan.com/learn> Dwinawan - UI/UX

- <https://chamjo.design/> Chamjo - UI/UX Inspirasi

- <https://mobbin.design/browse/android/apps> Mobbin - UI/UX

- <https://www.bookmarks.design/> Bookmarks - Resource UI/UX

- <https://www.lapa.ninja/> Lapa Ninja - UI/UX Landing Page

- <https://www.checklist.design/> checklist - UI/UX Component Page

- <https://blush.design/> Blush - UI/UX Ilustration

- <https://www.uidesigndaily.com/> UI/UX Desaign

#### Mockup
- <https://freebiesui.com/> Freebiesui - Mockup Desain

#### Background

- <https://www.pexels.com/> Pexels - Background Wallpaper

- <https://freestocktextures.com/> Free Stock Textures - Background Texture

- <http://texturelib.com/>Texture Lib - Background Texture

#### SVG
- <https://www.svgrepo.com/> SVG repo - Vector and Icon

- <https://illlustrations.co/> illlustrations - Vector and illustration

#### Animasi
- <https://lordicon.com/icons> lordicon - Animasi icon

#### Warna Inspirasi
- <https://www.0to255.com/> oto255 - Color Picker Tool

#### Tool Cepat
- <https://ray.so/> Ray - Membuat Kode Program Cantik

- <https://blobs.app/> blobs - Membuat blops

- <https://getwaves.io/> Get Waves - generator waves svg

#### Video
- <https://pixabay.com/sound-effects/> pPxabay - Sound Effects

